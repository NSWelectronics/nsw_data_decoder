#ifndef NSW_VMM_PACKET_H
#define NSW_VMM_PACKET_H

#include <iostream>
#include <string>
#include <sstream>
#include <cstdint>

namespace nsw {

    class VMMHeader {
        public :
            VMMHeader(){clear();}
            virtual ~VMMHeader(){};

            void clear() {
                rocid=0;
                timeset=0;
                orbit_count=0;
                bcid=0;
                decoded_bcid=0;
                level1Id=0;
                null_sop=0;
                null_eop=0;
                sop=0;

                // VMM
                art_trigger=0;
                art_valid=0;
                art_address=0;
                trigger_counter=0;
                trigger_timestamp=0;
                precision_counter=0;
                chip_number=0;
            }

            // ROC NULL
            uint32_t rocid;

            // ROC HIT
            uint32_t timeset;
            uint32_t orbit_count;
            uint32_t bcid;
            uint32_t decoded_bcid;
            uint32_t level1Id;

            uint32_t null_sop;
            uint32_t null_eop;
            uint32_t sop;

            // VMM
            uint32_t art_trigger;
            uint32_t art_valid;
            uint32_t art_address;
            uint32_t trigger_counter;
            uint32_t trigger_timestamp;
            uint32_t precision_counter; 
            uint32_t chip_number;

            std::string str() {
                std::stringstream out;
                out << "HIT sop=0x"<<std::hex<<sop<< " timeset="<<timeset
                    <<" orbit_count="<<orbit_count<<" bcid=0x"<<std::hex<<bcid
                    << " level1Id=0x"<<std::hex<<level1Id;
                return out.str();
            }
            std::string nullstr() {
                std::stringstream out;
                out << "NULL sop=0x"<<std::hex<<null_sop<<" rocid=0x"<<std::hex<<rocid
                    << " level1Id=0x"<<std::hex<<level1Id<<" eop=0x"<<null_eop;
                return out.str();
            }

    };

    class VMMHit {
        public :
            VMMHit() { clear(); }
            virtual ~VMMHit(){};

            void clear() {
                vmmid=0;
                channel=0;
                pdo=0;
                tdo=0;
                vmm_bcid=0;
                vmm_decoded_bcid=0;
                vmm_flag=0;
                vmm_pass_threshold=0;
                
            }

            uint32_t vmmid;
            uint32_t channel;
            uint32_t pdo;
            uint32_t tdo;
            uint32_t vmm_bcid;
            uint32_t vmm_decoded_bcid;
            uint32_t vmm_flag;
            uint32_t vmm_pass_threshold;

            // ROC + L0
            uint32_t relbcid;


            std::string str() {
                std::stringstream out;
                out << std::dec << "vmmid="<<vmmid<< std::dec << " channel="<<channel
                    <<" pdo="<<pdo<<" tdo=" << tdo
                    <<std::hex<<" relbcid="<<relbcid<<std::dec;
                return out.str();
            }
    };

    class VMMTrailer {
        public :
            VMMTrailer() { clear(); }
            virtual ~VMMTrailer(){};

            void clear() {
                e_flag=0;
                to_flag=0;
                missing_data_flag=0;
                level0Id=0;
                num_hits=0;
                checksum=0;
                eop=0;
            }
            uint32_t e_flag;
            uint32_t to_flag;
            uint32_t missing_data_flag;
            uint32_t level0Id;
            uint32_t num_hits;
            uint32_t checksum;
            uint32_t eop;

            std::string str() {
                std::stringstream out;
                out << std::hex << "e_flag=0x"<<e_flag<<" to_flag=0x"<<to_flag
                    << " missing data flags=0x" << missing_data_flag
                    << " l0Id=0x" << level0Id << std::dec << " num_hits=" << num_hits
                    << " eop=0x" << std::hex << eop
                    << " checksum=0x" << checksum << std::dec;
                return out.str();
            }
    };

    class VMMPacket {

        public :
            VMMPacket(size_t packet_length = 0/*bytes*/) {
                length = packet_length;
            }
            virtual ~VMMPacket(){};

            size_t length;

            void clear() {
                ok = false;
                header.clear();
                hit_data.clear();
                trailer.clear();
            }
            bool ok; 
            bool good() { return (ok && length>0); } 
            bool is_null;

            VMMHeader header;
            std::vector< VMMHit > hit_data;
            VMMTrailer trailer;

            void print() {
                std::cout << "VMMPacket    header : " << (is_null? header.nullstr() : header.str()) << std::endl;
                if(!is_null) {
                    for(size_t ihit = 0; ihit < hit_data.size(); ihit++)
                    std::cout << "VMMPacket    hit["<<ihit<<"] : " << hit_data.at(ihit).str() << std::endl;
                    std::cout << "VMMPacket    trailer : " << trailer.str() << std::endl;
                }

            }

    };

}

#endif
