#ifndef NSW_ROC_DECODER_H
#define NSW_ROC_DECODER_H

// std/stl
#include <vector>

// decoder
#include "decoder_imp.h"
#include "bit_manip.h"

namespace nsw {

    class ROCDecoder : public DecoderImp {

        public :
            ROCDecoder();
            virtual ~ROCDecoder(){};

            bool decode(const std::vector<uint8_t>& data, VMMPacket& packet);

    };


} // namespace

#endif
