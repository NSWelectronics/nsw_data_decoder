#ifndef NSW_VMM3_DECODER_H
#define NSW_VMM3_DECODER_H

// std/stl
#include <vector>

// decoder
#include "decoder_imp.h"

namespace nsw {

    class VMM3Decoder : public DecoderImp {

        public :
            VMM3Decoder();
            virtual ~VMM3Decoder(){};

            bool decode(const std::vector<uint32_t>& data, VMMPacket& packet);
    };

} // namespace

#endif
