#ifndef NSW_DECODE_TYPE_H
#define NSW_DECODE_TYPE_H

#include <string>

namespace nsw {

    enum DecodeType {
        VMMContinuous=0,
        VMML0,
        ROC,
        DECODEINVALID
    };

    // human readable
    std::string DecodeType2Str(const DecodeType &type);


} // namespace

#endif
