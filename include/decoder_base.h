#ifndef DECODER_BASE_H
#define DECODER_BASE_H


// std/stl
#include <vector>
#include <iostream>

// decoder
#include "data_type.h"
#include "decode_type.h"
#include "decoder_imp.h"
#include "vmm_packet.h"

namespace nsw {

    class DecoderBase {

        public :
            DecoderBase();
            virtual ~DecoderBase();

            bool set_decoding(DecodeType type);

            DecodeType decode_type() { return m_decode_type; }
            DataType data_type() { return m_type; }

            virtual void clear_data();
            virtual void dump_data();

            virtual bool decode(std::vector<uint32_t> data, VMMPacket& packet);
            virtual bool decode(std::vector<uint8_t> data, VMMPacket& packet);

            virtual bool collect(std::vector<uint32_t> data);
            virtual bool collect(std::vector<uint8_t> data);


        private :

            DecodeType m_decode_type;
            DataType m_type;
            std::vector<uint32_t> m_data32;
            std::vector<uint8_t> m_data8;

            // decoder
            DecoderImp* m_decoder;


    }; // class

} // namespace

#endif
