#ifndef NSW_DATA_TYPE_H
#define NSW_DATA_TYPE_H

#include <string>
#include <vector>
#include <typeinfo>

namespace nsw {

    enum DataType {
        unsigned32=0,
        unsigned8,
        DATAINVALID
    };

    // human readable
    std::string DataType2Str(const DataType &type);

    // test
    template<class T>
    DataType get_type(T input) {
        if(typeid(input).name() == typeid(std::vector<uint32_t>).name()) {
            return DataType::unsigned32;
        }
        else if(typeid(input).name() == typeid(std::vector<uint8_t>).name()) {
            return DataType::unsigned8;
        }
        else {
            return DataType::DATAINVALID;
        }
    }

} // namespace

#endif
