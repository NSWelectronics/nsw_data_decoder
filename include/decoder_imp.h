#ifndef NSW_DECODER_IMP_H
#define NSW_DECODER_IMP_H

#include <vector>
#include <assert.h>

#include "vmm_packet.h"
#include <cstdint>
#include <limits>

#define CHAR_BIT 8

namespace nsw {

    template <typename T>
    T reverse(T n, size_t b = sizeof(T) * CHAR_BIT)
    {
        assert(b <= std::numeric_limits<T>::digits);
    
        T rv = 0;
    
        for (size_t i = 0; i < b; ++i, n >>= 1) {
            rv = (rv << 1) | (n & 0x01);
        }
    
        return rv;
    }

    class DecoderImp {

        public :
            DecoderImp();
            virtual ~DecoderImp(){};

            virtual bool decode(const std::vector<uint32_t>& data, VMMPacket& packet);
            virtual bool decode(const std::vector<uint8_t>& data, VMMPacket& packet);

            // utility
            uint32_t decode_gray(uint32_t graycode);

    };


} // namespace

#endif
