# nsw_data_decoder


## Introduction
**nsw_data_decoder** is a library that encapsulates a prototype of the decoding algorithm
used to decode the NSW data format from the NSW front end electronics.


The assumed data format from the front end electronics is that of the ROC ASIC output, which
is received and forwarded to FELIX by the L1DDC, as specified in the Fig. 9 of the [VMM3+ROC specifications][1].

## Compilation

1) Check out this repository. There are not yet any recommended releases, so grab the master. Check it out in a relevant-to-NSW-readout directory.

2) Follow the usual *CMake* steps to compile and build the library:

```
cd nsw_data_decoder/
mkdir build/
cd build/
cmake ..
make
```

Once the compilation completes, the shared object ```NSWDecoder``` will be created in the *build/* directory.

The compilation will also produce an executable file "decoder_example". Run it as follows: ```./decoder_example```.

## Example Use

In order to use this decoding library, you must follow the compilation steps listed above.

You must then link to this library by adding the appropriate link options in your software's compilation procedure. For example, in a *CMake* based compilation,
in your CMakeLists.txt file you would have some lines along the lines of:

```
set(DECODE_INCLUDE_DIR $ENV{NSW_DECODE_DIR}/include)
set(DECODE_LIBS_DIR $ENV{NSW_DECODE_DIR}/build/)
include_directories(${DECODE_INCLUDE_DIR})
...
target_link_libraries(<target-name> -L${DECODE_LIBS_DIR} -lNSWDecoder)
```

where **NSW_DECODE_DIR** is the directory ```<full-path-to>/nsw_data_decoder/```.


In your application that uses this decoding, you must then include the file *nsw_decoder_include.h*,

```
#include "nsw_decoder_include.h"
```

and in the relevant header/source have an object of type ```nsw::DecoderBase```, which has a default constructor.

In the current implmentation you must set the decoding type of the decoder. The only one in place is that of the ROC/NSW decoding and can be set using the ```set_decoding``` method. These steps are shown here:

```
nsw::DecoderBase* decoder = new nsw::DecoderBase();
if(!decoder->set_decoding(nsw::DecodeType::ROC)) {
    cout << "Woops!" << endl;
}
```

To use the decoder, you must initialize an object of type ```nsw::VMMPacket``` with the length (in bytes) of the data packet. Currently, the decoder handles data packages in a ```std::vector<uint8_t>```. This is seen here, where
we decode the data packet using the decoder's ```decode``` method:

```
nsw::VMMPacket* packet = new nsw::VMMPacket(data.size());
if(!decoder->decode(data, *packet)) {
    cout << "Woops, bad data!" << endl;
}

if(packet->good()) {
    /* do stuff with the data here */
}
```

A few more details can be found in the [example code][2] (this is run with the executavble **decoder_example**).

The decoded data is placed in the **nsw::VMMPacket** data structure. You can [inspect this structure][3] to find out what is being decoded and how to access it.

## Questions/Comments

Contact: **Daniel Antrim**: <daniel.joseph.antrim@cern.ch>


[1]: https://twiki.cern.ch/twiki/pub/Atlas/NSWelectronics/VMM3_ROCspec.pdf
[2]: https://gitlab.cern.ch/NSWelectronics/nsw_data_decoder/blob/master/src/main_example.cpp
[3]: https://gitlab.cern.ch/NSWelectronics/nsw_data_decoder/blob/master/include/vmm_packet.h