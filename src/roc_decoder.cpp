#include "roc_decoder.h"

// std/stl
#include <iostream>
#include <bitset>
using namespace std;

// nsw
#include "vmm_packet.h"
using namespace nsw;


/////////////////////////////////////////////////////////////////////////
ROCDecoder::ROCDecoder()
{
    cout << " >> ROCDecoder" << endl;
}
/////////////////////////////////////////////////////////////////////////
bool ROCDecoder::decode(const std::vector<uint8_t>& data, VMMPacket& packet)
{
    bool status = true;
    if(!(packet.length > 0)) {
        cout << "ROCDecoder::decode    VMMPacket length is zero, initialize before decoding!" << endl;
        return false;
    }
    VMMHeader header;
    VMMTrailer trailer;

    cout << "incoming data (size=" << data.size()<<") = ";
    for(auto& x : data) {
        cout << std::hex << " " << unsigned(x);
    }
    cout << endl;

    //////////////////////////////////////////////////////////////
    // NULL or EMPTY event is 32-bits long
    //////////////////////////////////////////////////////////////
    if(data.size()==4) {
        packet.is_null = true;
        uint8_t sop = data.at(0);
        uint8_t eop = data.at(3);
        uint8_t h0 = data.at(1);
        uint8_t h1 = data.at(2);

        header.rocid = (( 0xfc & h0 ) >> 2);
        header.level1Id = h1;

        //reverse
        uint32_t hh = 0x0;
        uint32_t hh0 = nsw::reverse(sop);
        uint32_t hh1 = nsw::reverse(h0);
        uint32_t hh2 = nsw::reverse(h1);
        uint32_t hh3 = nsw::reverse(eop);
        hh |= hh0;
        hh |= ( hh1 << 8 );
        hh |= ( hh2 << 16 );
        hh |= ( hh3 << 24);

        header.rocid = nsw::reverse( 0xfc00 & hh ) >> 16;
        header.level1Id = nsw::reverse( 0xff0000 & hh ) >> 8;

        header.null_sop = sop;
        header.null_eop = eop;

        packet.header = header;

        packet.ok = status;
        return status;
    }

    //////////////////////////////////////////////////////////////
    // here we have HIT data
    //////////////////////////////////////////////////////////////
    packet.is_null = false;

    uint8_t sop = data.at(0);
    uint8_t eop = data.at(data.size()-1);


    if(!(sop==0x3c)) {
        status = false;
        cout << "SOP not found" << endl;
    }
    if(!(eop==0xdc)) {
        status = false;
        cout << "EOP not found" << endl;
    }
    size_t minimum_number_of_words = 3; // 32-bit words: hit header, hit data, hit trailer [not including SOP/EOP]
    size_t num32 = (data.size() - 2) / 4; // 8-bit unpacking, removing EOP/SOP
    if( (num32 < minimum_number_of_words) || ((num32*4)%4!=0) ) {
        cout << std::dec <<  "HIT data has bad number of bytes (got=" << (data.size()-2) << ", expect>=" << (minimum_number_of_words*4) << ") [num32="<<num32<< "]" << endl;
        status = false;
    }

    packet.ok = status;
    // don't worry about decoding if it is malformed
    if(!status) return status;

    size_t eop_idx = (data.size()-1);
    vector<uint32_t> payload;
    uint32_t dp = 0x0;
    uint32_t d0 = 0x0;
    uint32_t d1 = 0x0;
    uint32_t d2 = 0x0;
    uint32_t d3 = 0x0;
    for(size_t idx = 1; idx < eop_idx; idx+=4) {
        dp = 0x0;
        d0 = data.at(idx);
        d1 = data.at(idx+1);
        d2 = data.at(idx+2);
        d3 = data.at(idx+3);

        //reverse
        d0 = nsw::reverse(data.at(idx));
        d1 = nsw::reverse(data.at(idx+1));
        d2 = nsw::reverse(data.at(idx+2));
        d3 = nsw::reverse(data.at(idx+3));
        dp |= ( d0 );
        dp |= ( d1 << 8 );
        dp |= ( d2 << 16 );
        dp |= ( d3 << 24 );
        payload.push_back(dp);
    }

    if(!(payload.size()>2)) {
        cout << "Payload size for non-null packet is size <2! (size=" << data.size() << ")" << endl;
    }

    // fill the header, hits, and trailer regions
    uint32_t header32 = payload.at(0);
    vector<uint32_t> hits32;
    for(size_t hitidx = 1; hitidx < (payload.size()-1); hitidx++) hits32.push_back(payload.at(hitidx));
    uint32_t trailer32 = payload.at(payload.size()-1);

    uint32_t num_hits = ((0xffc000 & trailer32) >> 14);
    //reverse
    num_hits = nsw::reverse( 0xffc000 & trailer32 ) >> 8;
    if(num_hits != hits32.size()) {
        cout << std::dec << "num_hits from VMMPacket trailer does not match number of hits unloaded! (num_hits=" << num_hits << std::dec << "  hits32=" << hits32.size() << ")"  << endl;
    }
    packet.hit_data.clear();

    // fill the header
    header.timeset = ( header32 & 0x1 );
    header.orbit_count = (( header32 & 0xc ) >> 2);
    header.bcid = (( header32 & 0xfff0 ) >> 4);
    header.decoded_bcid = DecoderImp::decode_gray(header.bcid);
    header.level1Id = (( header32 & 0xffff0000 ) >> 16);

    //reverse
    header.timeset = nsw::reverse( header32 & 0x1 ) >> 31;
    header.orbit_count = nsw::reverse( header32 & 0xc ) >> 30;
    header.bcid = nsw::reverse( header32 & 0xfff0 ) >> 16;
    header.decoded_bcid = DecoderImp::decode_gray(header.bcid);
    header.level1Id = nsw::reverse( header32 & 0xffff0000 );

    // fill the hits
    VMMHit hit;
    for(auto& hitdata : hits32) {
        hit.clear();
        hit.relbcid = (( 0x1c & hitdata ) >> 2);
        hit.vmmid = (( 0xe0 & hitdata) >> 5);
        hit.channel = (( 0x3f00 & hitdata ) >> 8);
        hit.pdo = (( 0xffc000 & hitdata ) >> 14);
        hit.tdo = (( 0xff000000 & hitdata ) >> 24);


        //reverse
        hit.relbcid = nsw::reverse( 0x1c & hitdata ) >> 27;
        hit.vmmid = nsw::reverse( 0xe0 & hitdata ) >> 24;
        hit.channel = nsw::reverse( 0x3f00 & hitdata ) >> 18;
        hit.pdo = nsw::reverse( 0xffc000 & hitdata ) >> 8;
        hit.tdo = nsw::reverse( 0xff000000 & hitdata );
        packet.hit_data.push_back(hit);
    }

    // fill the trailer
    trailer.e_flag = (( 0x1 & trailer32 ));
    trailer.to_flag = (( 0x2 & trailer32 ) >> 1);
    trailer.missing_data_flag = (( 0x3fc & trailer32 ) >> 2);
    trailer.level0Id = (( 0x3c00 & trailer32 ) >> 10);
    trailer.num_hits = num_hits;
    trailer.checksum = (( 0xff000000 & trailer32 ) >> 24);

    //reverse
    trailer.e_flag = nsw::reverse( 0x1 & trailer32 ) >> 31;
    trailer.to_flag = nsw::reverse( 0x2 & trailer32 ) >> 30;
    trailer.missing_data_flag = nsw::reverse( 0x3fc & trailer32 ) >> 22;
    trailer.level0Id = nsw::reverse( 0x3c00 & trailer32 ) >> 18;
    trailer.num_hits = num_hits; // decoded earlier and correctly
    trailer.checksum = nsw::reverse( 0xff000000 & trailer32 );
    

    header.sop = sop;
    trailer.eop = eop;

    packet.header = header;
    packet.trailer = trailer;
    packet.ok = status;
    packet.print();
    cout << " >> packet status = " << packet.ok << "  (good=" << packet.good() << ")" << endl;
    return status;
}
