#include "decoder_imp.h"

// std/stl
#include <iostream>
using namespace std;


using namespace nsw;

///////////////////////////////////////////////////////////////////////////
DecoderImp::DecoderImp()
{
}
///////////////////////////////////////////////////////////////////////////
bool DecoderImp::decode(const std::vector<uint32_t>& data, VMMPacket& packet)
{
    cout << "DecoderImp::decode" << endl;
    packet.clear();
    return false;
}
///////////////////////////////////////////////////////////////////////////
bool DecoderImp::decode(const std::vector<uint8_t>& data, VMMPacket& packet)
{
    cout << "DecoderImp::decode" << endl;
    packet.clear();
    return false;
}
///////////////////////////////////////////////////////////////////////////
uint32_t DecoderImp::decode_gray(uint32_t gray)
{
    uint32_t mask;
    for( mask = gray >> 1; mask != 0; mask = mask >> 1) {
        gray = gray ^ mask;
    }
    return gray;
}
