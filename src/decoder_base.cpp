#include "decoder_base.h"

//std/stl
#include <iostream>
#include <string>
#include <typeinfo>
using namespace std;

// nsw
#include "roc_decoder.h"
using namespace nsw;

///////////////////////////////////////////////////////////////////////////
DecoderBase::DecoderBase() :
    m_decode_type(DecodeType::DECODEINVALID),
    m_type(DataType::DATAINVALID),
    m_decoder(nullptr)
{
    cout << "DecoderBase hello" << endl;
    clear_data();
}
///////////////////////////////////////////////////////////////////////////
DecoderBase::~DecoderBase()
{
    cout << "DecoderBase goodbye" << endl;
}
///////////////////////////////////////////////////////////////////////////
bool DecoderBase::set_decoding(DecodeType type)
{
    if(type==DecodeType::DECODEINVALID) return false;

    cout << "Decoder    Set decoding : " << DecodeType2Str(type) << endl;
    m_decode_type = type;

    if(m_decode_type == DecodeType::ROC) {
        m_decoder = new ROCDecoder();
    }
    else {
        cout << "Decoder    ERROR No decoder available for DecodeType : " << DecodeType2Str(type) << endl;
        return false;
    }

    return true;
}
///////////////////////////////////////////////////////////////////////////
void DecoderBase::clear_data()
{
    m_data32.clear();
    m_data8.clear();
}
///////////////////////////////////////////////////////////////////////////
bool DecoderBase::collect(vector<uint32_t> data)
{
    clear_data();
    if(decode_type()==DecodeType::ROC) {
        cout << "Decoder::collect    ERROR Decoding for ROC DecodeType requires 8-bit packing!" << endl;
        return false;
    }
    m_type = DataType::unsigned32;
    m_data32 = data;
    return true;
}
///////////////////////////////////////////////////////////////////////////
bool DecoderBase::collect(vector<uint8_t> data)
{
    clear_data();
    if(decode_type() < DecodeType::ROC) {
        cout << "Decoder::collect    ERROR Decoding 8-bit unpacking for DecodeType::" << DecodeType2Str(decode_type()) << " not available" << endl;
        return false;
    }
    m_type = DataType::unsigned8;
    m_data8 = data;
    return true;
}
///////////////////////////////////////////////////////////////////////////
void DecoderBase::dump_data()
{
    if(data_type() == DataType::unsigned8) {
        size_t n_dumped = 0;
        
        cout << "dump_data    0x ";
        for(auto& x : m_data8) {
            if(n_dumped%28==0 && n_dumped>0) cout << "\ndump_data    0x ";
            if(n_dumped%4==0 && n_dumped>0 && n_dumped%28!=0) cout << " ";
            cout << std::hex << unsigned(x) << " ";
            n_dumped++;
        }
        cout << "\n";
    }
    else if(data_type() == DataType::unsigned32) {
        size_t n_dumped = 0;
        cout << "dump_data    0x ";
        for(auto& x : m_data32) {
            if(n_dumped%10==0 && n_dumped>0) cout << "\ndump_data    0x ";
            cout << std::hex << x << " ";
            n_dumped++;
        }
        cout << "\n";
    }
    else if(data_type() == DataType::DATAINVALID) {
        cout << "dump_data    INVALID data" << endl;
    }

}
///////////////////////////////////////////////////////////////////////////
bool DecoderBase::decode(std::vector<uint32_t> data, VMMPacket& packet)
{
    if(!collect(data)) {
        packet.clear();
        return false;
    }

    m_decoder->decode(data, packet);
    if(!packet.good()) {
        // don't clear the packet so that the user can check it?
        //cout << "Decoder::decode    ERROR Decoding invalid" << endl;
        return false;
    }
    return true;
}
bool DecoderBase::decode(std::vector<uint8_t> data, VMMPacket& packet)
{
    if(!collect(data)) {
        packet.clear();
        return false;
    }
    m_decoder->decode(data, packet);
    if(!packet.good()) {
        // don't clear the packet so that the user can check it?
        //cout << "Decoder::decode    ERROR Decoding invalid" << endl;
        return false;
    }
    return true;
}
//    VMMPacket packet(0);
//    if(data_type()==DataType::unsigned8) {
//        packet = m_decoder->decode(m_data8);
//    }
//    else if(data_type()==DataType::unsigned32) {
//        packet = m_decoder->decode(m_data32);
//    }
//    if(!packet.ok())
//    cout << "Decoder    ERROR No valid type set for decoding" << endl;
//    return packet;
//
//}
