#include "decode_type.h"

// std/stl
#include <string>
using namespace std;

namespace nsw {

string DecodeType2Str(const DecodeType& type)
{
    string s = "INVALID";
    if(type==DecodeType::VMMContinuous) s = "VMMContinuous";
    else if(type==DecodeType::VMML0) s = "VMML0";
    else if(type==DecodeType::ROC) s= "ROC";
    return s;
}

} // namespace
