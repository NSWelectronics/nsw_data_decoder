#include "data_type.h"

// std/stl
#include <string>
using namespace std;

namespace nsw {

///////////////////////////////////////////////////////////////////////////
string DataType2Str(const DataType& type)
{
    string s = "INVALID";
    if(type==DataType::unsigned32)  s = "vector<uint32_t>";
    else if(type==DataType::unsigned8)  s = "vector<uint8_t>";
    return s;
}



} // namespace

