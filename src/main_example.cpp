//#include "decoder_base.h"
//#include "decode_type.h"
//#include "vmm_packet.h"
#include "nsw_decoder_include.h"

//std/stl
#include <iostream>
#include <string>
#include <cstdint>
#include <vector>
using namespace std;


int main(int argc, char* argv[])
{
    cout << " ** decoder example ** " << endl;

    // Declare and initialize the decoder 
    nsw::DecoderBase* decoder = new nsw::DecoderBase();

    // We must tell the decoder what type of decoding to do
    // (c.f. nsw_data_decoder/decode_type.h).
    // The method 'set_decoding' returns true if the decoding
    // is valid and defiend, and false otherwise.
    // here we setup to decode the ROC/NSW-like output data format.
    if(!decoder->set_decoding(nsw::DecodeType::ROC)) return 1;

    // Here I create some dummy data to put into the decoder.
    uint8_t sop = 0x3c; // SOP - every packet from the ROC starts with this start of packet character
    vector<uint8_t> header =    { 0x00, 0x01, 0x00, 0x02 };
    vector<uint8_t> hit1 =      { 0x0a, 0x0c, 0x04, 0x05 };
    vector<uint8_t> hit2 =      { 0x37, 0x3c, 0x10, 0x11 };
    vector<uint8_t> trailer =   { 0x2a, 0x00, 0x02, 0x5b };
    uint8_t eop = 0xdc; // EOP - every packet from the ROC ends with this end of packet character
    vector<uint8_t> data;
    data.push_back(sop);
    for(auto x : header) data.push_back(x);
    for(auto x : hit1) data.push_back(x);
    for(auto x : hit2) data.push_back(x);
    for(auto x : trailer) data.push_back(x);
    data.push_back(eop);

    // Before decoding, create an empty VMMPacket structure which
    // will be filled in by the decoder. The VMMPacket structure
    // is initialized with the data size.
    nsw::VMMPacket* packet = new nsw::VMMPacket(data.size());

    // Pass the raw data and the packet object to the decoder's
    // 'decode' method. This method returns true or false,
    // depending on whether the decoding was successful or not. 
    if(!decoder->decode(data, *packet)) {
        cout << "ERROR There was a problem decoding the data packet!" << endl;
        return 1;
    }

    if(packet->good()) {
        // If the packet is good, let's print out its contents using the VMMPacket:print
        // method.
        packet->print();

        // Also can access the hit data, trailer, and hit header and access their
        // data fields.
        cout << std::dec << endl;
        cout << "----------------------------------------------" << endl;
        cout << " HEADER INFO " << endl;
        cout << " orbit count           : " << packet->header.orbit_count << endl;
        cout << " BCID                  : " << packet->header.bcid << endl;
        cout << " Level1ID              : " << packet->header.level1Id << endl;
        cout << endl;
        cout << " HIT INFO " << endl;
        int hit_no = 0;
        for(auto & hit : packet->hit_data) {
            cout << " hit["<<hit_no<<"]                : vmm = " << hit.vmmid << ", channel = " << hit.channel << ", pdo = " << hit.pdo << ", tdo = " << hit.tdo << endl;
            hit_no++;
        }
        cout << endl;
        cout << " TRAILER INFO " << endl;
        cout << " Level0Id              : " << packet->trailer.level0Id << endl;
        cout << "----------------------------------------------" << endl;
    }

    // clean up
    delete packet;
    delete decoder;

    return 0;
}
