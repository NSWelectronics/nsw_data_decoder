#include "vmm3_decoder.h"

// std/stl
#include <iostream>
using namespace std;

// nsw
#include "vmm_packet.h"
using namespace nsw;

/////////////////////////////////////////////////////////////////////////
VMM3Decoder::VMM3Decoder()
{
    cout << " >> VMM3Decoder" << endl;
}
/////////////////////////////////////////////////////////////////////////
bool VMM3Decoder::decode(const std::vector<uint32_t>& data, VMMPacket& packet)
{
    bool status = true;
    if(!(packet.length > 0)) {
        cout << "VMM3Decoder::decode    VMMPacket length is zero, initialize before decoding!" << endl;
        return false;
    }

    uint32_t trailer=0xffffffff;
    if(data.at(data.size()-1) != trailer) {
        cout << "VMM3Decoder::decode    Input data does not end with expected trailer (0x" << std::hex << trailer << std::dec << ")" << endl;
        packet.ok = false;
        return false;
    }


    //////////////////////////////////////////////////////////////
    // NULL or EMPTY event is 32-bit ART header + 2x32-bit VMM header + 32-bit trailer
    //////////////////////////////////////////////////////////////
    if(data.size()==4) {
        packet.is_null = true;
    }

    VMMHeader header;
    // no trailer for VMM3 decoding
    uint32_t art_header32 = data.at(0);
    uint32_t vmm_header32_0 = data.at(1);
    uint32_t vmm_header32_1 = data.at(2);

    header.art_trigger = (( 0xfc00 & art_header32 ) >> 10);
    header.art_valid = (( 0x100 & art_header32 ) >> 8);
    header.art_address = (( 0x3f & art_header32 ));

    header.trigger_counter = vmm_header32_0;
    header.precision_counter = (( vmm_header32_1 & 0xff000000 ) >> 24);
    header.trigger_timestamp = (( vmm_header32_1 & 0xffff00 ) >> 8);
    header.chip_number = (( vmm_header32_1 & 0xff ));

    packet.ok = status;
    if(packet.is_null) return status;

    packet.ok = status;
    if(!status) return status;

    vector<uint32_t> payload;
    size_t trailer_idx = (data.size()-1);
    for(size_t idx = 3; idx < trailer_idx; idx++) {
        payload.push_back(data.at(idx));
    } 

    if(!(payload.size()>=2)) {
        cout << "Payload size for non-null packet is size <2x32-bits! (size=" << data.size() << "x32-bits)" << endl;
        status = false;
    }
    packet.ok = status;
    return status;

    size_t hit_size = 2; // VMM3 hit data is 2x32 bits wide
    VMMHit hit;
    for(size_t hitidx = 0; hitidx < payload.size(); hitidx+=hit_size) {
        hit.clear();

        uint32_t d0 = payload.at(hitidx);
        uint32_t d1 = payload.at(hitidx+1);

        hit.pdo = ( d0 & 0x3ff );
        hit.vmm_bcid = (( d0 & 0x3ffc00 ) >> 10);
        hit.vmm_decoded_bcid = DecoderImp::decode_gray(hit.vmm_bcid); 
        hit.tdo = (( d0 & 0x3fc00000 ) >> 22);
        hit.vmm_flag = (( d1 & 0x1 ));
        hit.vmm_pass_threshold = (( d1 & 0x2 ) >> 1);
        hit.channel = (( d1 & 0xfc ) >> 2);
        packet.hit_data.push_back(hit);
        
    }
    
    

}
